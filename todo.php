<?php
require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/config.php';

session_start();

$user = $_SESSION['login'];
$user_id = $_SESSION['user_id'];
$assigned_user_id= $_SESSION['user_id'];
$description = (isset($_POST['addTask'])) ? $_POST['description'] : "";
$id = (isset($_POST['deleteTask'])) ? $_POST['id_task'] : "";
$is_done = (isset($_POST['doneTask'])) ? $_POST['id_task'] : "";
$assignedUser = (isset($_POST['assigned_user_id'])) ? $_POST['assigned_user_id'] : "";
$getId = (isset($_POST['get_id'])) ? $_POST['get_id'] : "";

if ($description){
    insertTask($db, $description, $user_id, $assigned_user_id);
}
elseif ($id){
    deleteTask($db, $id, $user_id);
}
elseif ($is_done){
    updateIsDone($db, $is_done, $user_id);
}
elseif ($assignedUser && $getId){
    updateAssignedUserId($db, $assignedUser, $getId, $user_id);
}
$myTasks = selectMyTasks($db, $user_id);
$allTasks = selectAllTasks($db);
$assignedUserList = joinTable($db);
$getAssignedTable = getAssignedTable($db);
$countTasks = countTasks($db, $user_id);
?>

<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="css/style.css" rel="stylesheet">
    <style>
    .colorgraph {
            height: 5px;
            border-top: 0;
            background: #c4e17f;
            border-radius: 5px;
            background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        }
    </style>
    <title>Список дел</title>
</head>
<body>
    <div class = "container">
        <div class="col-xs-12">
            <hr class="colorgraph"></hr>
            <h1>Список моих дел:</h1>
            <div class="form">
                <form method="POST" action="">
                    <input class="field" type="text" name="description" placeholder="Описание задачи" value="" >
                    <input type="submit" name="addTask" value="Добавить задачу">
                </form>
            </div>
            <br>
            <div class="form">
                <form method="POST" action="">
                    <input class="field" type="text" name="id_task" placeholder="Id задачи" value="" >
                    <input type="submit" name="deleteTask" value="Удалить задачу">
                    <input type="submit" name="doneTask" value="Задача выполнена">
                </form>
            </div>
            <br>
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Описание задачи</th>
                    <th>Дата добавления</th>
                    <th>Выполнена/Невыполнена</th>
                </tr>
                <?php foreach ($myTasks as $key => $val): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($val['description']); ?></td>
                        <td><?php echo $val['date_added']; ?></td>
                        <td><?php echo htmlspecialchars($val['is_done']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <hr class="colorgraph"></hr>
            <h1>Список всех дел:</h1>
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Описание задачи</th>
                    <th>Дата добавления</th>
                    <th>Выполнена/Невыполнена</th>
                    <th>Исполнитель</th>
                </tr>
                <?php foreach ($allTasks as $key => $val): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($val['description']); ?></td>
                        <td><?php echo $val['date_added']; ?></td>
                        <td><?php echo htmlspecialchars($val['is_done']); ?></td>
                        <td>
                            <form action="" method="POST">
                                <input type="hidden" name="get_id" value="<?php echo $val['id']; ?>">
                                <select name="assigned_user_id">
                                    <?php foreach ($assignedUserList as $assignedUser): ?>
                                        <option  value="<?= $assignedUser['user_id']?>">
                                            <?= $assignedUser['author'] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <input type="submit" name="getTask" value="Делегировать">
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <hr class="colorgraph"></hr>
            <h1>Список делегированных дел:</h1>
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Описание задачи</th>
                    <th>Дата добавления</th>
                    <th>Автор</th>
                    <th>Выполнена/Невыполнена</th>
                </tr>
                <?php foreach ($getAssignedTable as $key => $val): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($val['description']); ?></td>
                        <td><?php echo $val['date_added']; ?></td>
                        <td><?php echo htmlspecialchars($val['author']); ?></td>
                        <td><?php echo htmlspecialchars($val['is_done']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <hr class="colorgraph"></hr>
            <h1>Всего дел: <?php echo $countTasks[0]['count(*)'] ;?></h1>
            <div class="form">
                <form method="GET" action="index.php">
                    <input type="submit" name="exit" value="Выход">
                </form>
            </div>
        </div>
    </div>
</body>
</html>
