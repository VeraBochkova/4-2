<?php
function getUser($db, $login, $password) {
    $data = selectUser($db, $login, $password);
    if (empty($data)) {
        return FALSE;
    } else {
        return $data[0]['id'];
    }
}

function selectUser($db, $login, $password){
    $sql = "SELECT login, password, id FROM user WHERE login LIKE ? && password LIKE ?";
    $st = $db->prepare($sql);
    $st->execute(["$login", "$password"]);
    return $st->fetchAll(PDO::FETCH_ASSOC);
}

function getUserLogin($db, $login) {
    $data = selectUserLogin($db, $login);
    if (empty($data)) {
        return FALSE;
    } else {
        return $data[0]['id'];
    }
}

function selectUserLogin($db, $login){
    $sql = "SELECT login, password, id FROM user WHERE login LIKE ?";
    $st = $db->prepare($sql);
    $st->execute(["$login"]);
    return $st->fetchAll(PDO::FETCH_ASSOC);
}

function registerUser($db, $login, $password) {
    if ($login && $password) {
        insertUser($db, $login, $password);
    } else {
        die("<p style='color: red;'>Некорретный ввод логина/пароля <a href='index.php'>Войти/Зарегистрироваться</a></p>");
    }
}

function insertUser($db, $login, $password){
    $sql = "INSERT INTO user(login, password) VALUES (?, ?)";
    $st = $db->prepare($sql);
    $st->execute(["$login", "$password"]);
}

function sessionStart($login, $user_id) {
    session_start();
    $_SESSION['login'] = $login;
    $_SESSION['user_id'] = $user_id;
}

function insertTask($db, $description, $user_id, $assigned_user_id){
    $date_added = date('Y-m-d H:i:s');
    $sql = "INSERT INTO task (description, user_id, assigned_user_id, date_added) VALUES (?, ?, ?, ?)";
    $st = $db->prepare($sql);
    $st->execute(["$description", "$user_id", "$assigned_user_id", "$date_added"]);
}

function selectMyTasks($db, $user_id){
    $sql = "SELECT * FROM task WHERE user_id LIKE ? ORDER BY date_added";
    $st = $db->prepare($sql);
    $st->execute(["$user_id"]);
    return  $st->fetchALL(PDO::FETCH_ASSOC);
}

function selectAllTasks($db){
    $sql = "SELECT * FROM task";
    $st = $db->prepare($sql);
    $st->execute([]);
    return  $st->fetchALL(PDO::FETCH_ASSOC);
}

function deleteTask($db, $id, $user_id){
    $sql = "DELETE FROM task WHERE id LIKE ? AND user_id LIKE ? LIMIT 1";
    $st = $db->prepare($sql);
    $st->execute(["$id", "$user_id"]);
}

function updateIsDone($db, $is_done, $user_id){
    $sql = "UPDATE task SET is_done=1 WHERE id LIKE ? AND user_id LIKE ? LIMIT 1";
    $st = $db->prepare($sql);
    $st->execute(["$is_done", "$user_id"]);
    return  $st->fetchALL(PDO::FETCH_ASSOC);
}

function joinTable($db){
    $sql = "SELECT task.user_id, task.description, task.date_added, task.is_done, task.id, task.assigned_user_id, user.login AS author, user.password 
                      FROM task 
                      JOIN user ON user.id = task.user_id GROUP BY user_id";
    $st = $db->prepare($sql);
    $st->execute([]);
    return $st->fetchALL(PDO::FETCH_ASSOC);
}

function updateAssignedUserId($db, $assignedUser, $getId, $user_id){
    $sql = "UPDATE task SET assigned_user_id = $assignedUser WHERE id LIKE ? AND user_id LIKE ?";
    $st = $db->prepare($sql);
    $st->execute(["$getId", "$user_id"]);
}

function getAssignedTable($db){
    $sql = "SELECT task.user_id, task.description, task.date_added, task.is_done, task.id, task.assigned_user_id, user.login AS author, user.password 
                      FROM task 
                      JOIN user ON user.id = task.assigned_user_id ";
    $st = $db->prepare($sql);
    $st->execute([]);
    return $st->fetchALL(PDO::FETCH_ASSOC);
}

function countTasks($db, $user_id){
    $sql = "SELECT count(*) FROM task WHERE assigned_user_id = $user_id AND user_id = $user_id";
    $st = $db->prepare($sql);
    $st->execute(["$user_id"]);
    return $st->fetchALL(PDO::FETCH_ASSOC);
}