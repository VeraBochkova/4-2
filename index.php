<?php
require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/config.php';

$login = (!empty($_POST['login'])) ? $_POST['login'] : "";
$password = (!empty($_POST['password'])) ? $_POST['password'] : "";
$register = (!empty($_POST['register'])) ? $_POST['register'] : "";

$sql = "
   CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `assigned_user_id` int(11) DEFAULT NULL,
  `description` varchar(500) NOT NULL,
  `is_done` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
$statement = $db->prepare($sql);
$statement->execute([]);

if (isset($_POST['sign_in'])){
    $user_id = getUser($db, $login, $password);
    if ($user_id) {
        sessionStart($login, $user_id);
        header('Location: todo.php');
    } else {
        die("<p>Пользователь c таким логином/паролем не зарегистрирован <a href='index.php'>Войти/Зарегистрироваться</a></p>");
    }
}
elseif (isset($_POST['register']) && $login){
    $user_id = getUserLogin($db, $login);
    if ($user_id) {
        die("<p>Пользователь с таким именем уже существует <a href='index.php'>Войти</a></p>");
    } else {
        registerUser($db, $login, $password);
        sessionStart($login, $user_id);
        header('Location: todo.php');
    }
}

?>

<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="css/style.css" rel="stylesheet">
    <style>
        .colorgraph {
            height: 5px;
            border-top: 0;
            background: #c4e17f;
            border-radius: 5px;
            background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        }
    </style>
    <title>Вход/Регистрация</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-8 col-md-4 col-sm-offset-3 col-md-offset-4">
            <form role="form" action="index.php" method="POST" enctype="multipart/form-data">
                <h2>Вход / регистрация</h2>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <input type="text"  name="login" id="lg" class="form-control input-lg" placeholder="Логин">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <input type="text"  name="password" id="key"  class="form-control input-lg" placeholder="Пароль">
                        </div>
                    </div>
                </div>
                <hr class="colorgraph">
                <div class="row" >
                    <div class="col-xs-12 col-md-6">
                        <input type="submit" name="sign_in" class="btn btn-success btn-block btn-lg" value="Вход">
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <input type="submit" name="register" class="btn btn-primary btn-block btn-lg" value="Регистрация">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
